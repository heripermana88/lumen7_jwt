<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'username' => 'admin', 
            'email' => 'admin@gmail.com',
            'password' => app('hash')->make('12345678'),
        ]);

        $admin_role = Role::create(['name' => 'Admin']);
        $permissions = Permission::pluck('id','id')->all();
        $admin_role->syncPermissions($permissions);
        $admin->assignRole([$admin_role->id]);
        
        // =====================================================
        $registrar = User::create([
            'username' => 'user', 
            'email' => 'user@gmail.com',
            'password' => app('hash')->make('12345678'),
        ]);

        $registrar_role = Role::create(['name' => 'User']);
        $permissions = Permission::pluck('id','id')->all();
        $registrar_role->syncPermissions($permissions);
        $registrar->assignRole([$registrar_role->id]);
        
    }
}
