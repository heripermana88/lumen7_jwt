<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Queue;
use App\Jobs\testQueue;

class sampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function test_queue() {
        for($i=0; $i<=10; $i++){
            // Queue::push('testing',new testQueue(array('queue' => $i)));
            dispatch((new testQueue(array('queue' => $i)))->onQueue('testQueue'));
            echo "successfully push";
        }
    }
}
