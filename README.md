# Lumen17 VueJS

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Contributing

Thank you for considering contributing to Lumen! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


Clone This Repo

composer update

composer require chuckrincon/lumen-config-discover

composer require tymon/jwt-auth:dev-develop

php artisan jwt:secret

php artisan migrate

## Run App

```
POST
api/register
request{
    username:permana
    password:123456789
    password_confirmation:123456789
}
response{
    "entity": "users",
    "action": "create",
    "result": "success"
}
```

```
POST
api/login
request{
    username:permana
    password:123456789
}

response
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXBpXC9sb2dpbiIsImlhdCI6MTU5OTkyMzU4OSwiZXhwIjoxNTk5OTI3MTg5LCJuYmYiOjE1OTk5MjM1ODksImp0aSI6IjRCYkZCZnBsalFKZk9Sd0MiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.zAFhPdhkhUHezgE7pQcMjt6H18IBdTkoG2f3yNOo8n0",
    "token_type": "bearer",
    "expires_in": null
}
```

```
GET
api/me
header{
    Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXBpXC9sb2dpbiIsImlhdCI6MTU5OTkyMzU4OSwiZXhwIjoxNTk5OTI3MTg5LCJuYmYiOjE1OTk5MjM1ODksImp0aSI6IjRCYkZCZnBsalFKZk9Sd0MiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.zAFhPdhkhUHezgE7pQcMjt6H18IBdTkoG2f3yNOo8n0
}

reponse{
    "id": 1,
    "username": "permana",
    "created_at": "2020-09-12T15:05:09.000000Z",
    "updated_at": "2020-09-12T15:05:09.000000Z"
}
```



## SPatie Role Permission

```
composer dump-autoload

--class=CreatePermissionSeeder
--class=CreateAdminUserSeeder

```


## Beanstalk Queue

```
sudo yum install beanstalkd
systemctl start beanstalkd
systemctl status beanstalkd
systemctl enable beanstalkd
sudo /usr/bin/beanstalkd -l 127.0.0.1 -p 11300

on .env
QUEUE_CONNECTION=beanstalkd

```